

# ThiniSTer - a pixel font with aesthetics borrowed from the Atari ST


## Examples

A perfect day, spent mostly in ViM.

![screenshot](vim_code.png)

When reviewing Git logs, the bold variant makes tags and branches stand out.

![screenshot](git_log.png)

Sometimes, one needs to swallow larger bodies of text.

![screenshot](goa_help.png)


## Installation

1. Copy the .bdf files into your home-local font directory:

    ```
    mkdir -f ~/.local/share/fonts/thinister
    cp 12.bdf 12-bold.bdf ~/.local/share/fonts/thinister
    ```

2. Tell fontconfig to re-scan the fonts

    `fc-cache -f`

3. Check that fontconfig has recognised the new font

    `fc-list | grep ThiniSTer`

Commodity Linux distributions tend to not enable support for bitmap fonts by
default. To use BDF fonts, you may need to tweak your /etc/fonts/.


## Usage in the terminal

Recent versions of the Gnome terminal and descendants apparently no longer
support BDF fonts since the switch of the font renderer from FreeType to
HarfBuzz. Fortunately, traditional terminal emulators like urxvt work fine.

For reference, the examples above were taken with urxvt and the following
settings in `~/.Xresources`.

  ```
  Rxvt.scrollBar:      false
  Rxvt.saveLines:      30000
  Rxvt.borderLess:     true
  Rxvt.borderColor:    rgb:0/0/0
  Rxvt.internalBorder: 1
  Rxvt.lineSpace:      3
  Rxvt.font:           xft:ThiniSTer:pixelsize=16
  Rxvt.fading:         12
  Rxvt.geometry:       108x34
  Rxvt.color0:         rgb:0/0/0
  Rxvt.color1:         rgb:d3/5c/5c
  Rxvt.color2:         rgb:9d/c6/7d
  Rxvt.color3:         rgb:b3/9a/69
  Rxvt.color4:         rgb:a8/a8/cb
  Rxvt.color5:         rgb:a5/7e/9b
  Rxvt.color6:         rgb:0/aa/aa
  Rxvt.color7:         rgb:aa/aa/aa
  Rxvt.color8:         rgb:55/55/55
  Rxvt.color9:         rgb:cc/55/55
  Rxvt.color10:        rgb:7a/ad/72
  Rxvt.color11:        rgb:ff/ff/55
  Rxvt.color12:        rgb:76/86/bd
  Rxvt.color13:        rgb:ff/55/ff
  Rxvt.color14:        rgb:77/ba/ac
  Rxvt.color15:        rgb:ff/ff/ff
  Rxvt.background:     rgb:18/21/2b
  Rxvt.foreground:     rgb:db/d5/d1
  ```


## Limitations

- Non-ASCII unicode coverage is poor.
  Only a few German characters received attention.

- There is only a single font size that may not work well on high-DPI displays.


## Credits and license

The font is based on the [Atari-ST font](https://github.com/ntwk/atarist-font),
which is used almost unmodified for the bold variant. The regular variant was
created by Norman Feske using gbdfedit.

Public domain.
